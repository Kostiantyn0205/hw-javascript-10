const tabsContainer = document.querySelector('.tabs');
const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

tabContents.forEach((content, index) => {
    if (index !== 0) {
        content.style.display = 'none';
    }
});
tabs[0].classList.add('active');

tabsContainer.addEventListener('click', (event) => {
    const clickedTab = event.target.closest('.tabs-title');
    if (!clickedTab) return;

    const tabIndex = Array.from(tabs).indexOf(clickedTab);

    tabs.forEach((tab) => {
        tab.classList.remove('active');
    });
    tabContents.forEach((content) => {
        content.style.display = 'none';
    });

    tabContents[tabIndex].style.display = 'block';
    clickedTab.classList.add('active');
});
